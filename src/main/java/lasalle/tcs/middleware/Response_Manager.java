package lasalle.tcs.middleware;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.*;
import java.net.*;

import org.json.simple.*;
import org.json.simple.parser.*;

public class Response_Manager {
	
	static JSONParser jsonParser = new JSONParser();
	
	public static JSONObject fromCache(String query,String[] APIs) {
		
		return Cache_Manager.getResponse(query, APIs);
		
	}
	
	public static JSONObject newResponse(String query,String[] APIs) {
		
		URL url = null;
		
		JSONObject JSONresponse = new JSONObject();
		
		JSONArray responses = new JSONArray();
		
		for(int m=0;m<APIs.length;m++) {
			try {
				url = new URL(APIs[m]);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("GET");
				
				JSONObject response = (JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				responses.add(response);

			} catch (IOException | ParseException e) {
				JSONresponse.put("status","UNKNOWN_ERROR");
				e.printStackTrace();
				return JSONresponse;
			}
		}
		
		JSONresponse.put("status","OK");
		JSONresponse.put("consultedWORD",query);
		JSONresponse.put("responses",DS_Validator.validateAPIResponses(responses));
		
		if(DS_Validator.validateResponse(JSONresponse)) {
			Cache_Manager.addResponse(query, APIs, JSONresponse.toString());
			return JSONresponse;
		}else {
			JSONresponse.clear();
			JSONresponse.put("status","UNKNOWN_ERROR");
			return JSONresponse;
		}
		
	}
	
}
