package lasalle.tcs.middleware;

import java.io.*;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.*;
import org.json.simple.parser.*;

import com.google.cloud.storage.*;

@WebServlet(
	    name = "Middleware",
	    urlPatterns = {"/middleware"}
	)

public class Request_Manager extends HttpServlet {

	@Override
	  public void doPost(HttpServletRequest request, HttpServletResponse response) 
	      throws IOException {
		
		JSONParser jsonParser = new JSONParser();
		JSONObject requestBODY = new JSONObject();
		
		response.addHeader("Content-Type", "application/json; charset=UTF-8");
		
		try {
			requestBODY = (JSONObject)jsonParser.parse(
					new InputStreamReader(request.getInputStream(),"UTF-8")
					);
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		
		JSONObject JSONResponse = new JSONObject();
				
		if(DS_Validator.validateRequest(requestBODY)) {
			
			String query = requestBODY.get("query").toString();
			String key = requestBODY.get("key").toString();
			
			String[] APIURLs = API_Validator.validate(key);
			
			//if APIURLs length >0
			
			if(Cache_Manager.inCache(query, APIURLs)) {
				JSONResponse = Response_Manager.fromCache(query,APIURLs);
				response.setStatus(HttpServletResponse.SC_OK);
				response.getWriter().print(JSONResponse);
				return;
			}else {
				JSONResponse = Response_Manager.newResponse(query,APIURLs);
				response.setStatus(HttpServletResponse.SC_OK);
				response.getWriter().print(JSONResponse);
				return;
			}

		}else {
			
			JSONResponse.put("status","INVALID_PARAMETERS");
			//put(Responses:[])
			
			if(DS_Validator.validateResponse(JSONResponse)) {
				response.setStatus(HttpServletResponse.SC_OK);
				response.getWriter().print(JSONResponse);
				return;
			}
			
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
			
		}
	  }
	
}
