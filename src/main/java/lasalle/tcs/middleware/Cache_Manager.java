package lasalle.tcs.middleware;

import java.util.Arrays;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.cloud.datastore.*;
import com.google.cloud.datastore.Query.ResultType;

public class Cache_Manager {

	static // Instantiates a client
    Datastore datastore = DatastoreOptions.getDefaultInstance().getService();

	public static boolean inCache(String query,String[] APIs) {

		String gqlQuery = "select __key__ from Cache where query = "+query+" and APIs = "+Arrays.toString(APIs) ;
		Query<Query.ResultType<Key>> dataStoreQuery = (Query<ResultType<Key>>) Query.newGqlQueryBuilder(gqlQuery).build();
		QueryResults<Query.ResultType<Key>> results = datastore.run(dataStoreQuery);
		
		if(results.hasNext()&&results.next()!=null) {
			return true;
		}
		
		return false;
		
	}
	
	public static JSONObject getResponse(String query,String[] APIs) throws ParseException {
		
		JSONParser jsonParser = new JSONParser();
		
		if(inCache(query,APIs)) {
			
			try {
				
				String gqlQuery = "select __key__ from Cache where query = "+query+" and APIs = "+Arrays.toString(APIs) ;
				Query<Key> dataStoreQuery = (Query<Key>) Query.newGqlQueryBuilder(gqlQuery).build();
				QueryResults<Key> results = datastore.run(dataStoreQuery);
				
				Entity cachedResponseEntity = datastore.get(results.next());
				
				int currentPriority = Integer.parseInt(cachedResponseEntity.getValue("priority").toString());
				
				cachedResponseEntity = Entity.newBuilder(cachedResponseEntity).set("priority",currentPriority+1).build();
				datastore.update(cachedResponseEntity);
				
				return (JSONObject) jsonParser.parse(cachedResponseEntity.getValue("response").toString());
				
			}catch(Exception e) {
				System.out.println(e);
				return new JSONObject();
			}
			
		}
		
		return new JSONObject();
		
	}

	public static void addResponse(String query,String[] APIs,String response) {
		
		 KeyFactory keyFactory = datastore.newKeyFactory().setKind("Cache");
		 Key key = keyFactory.newKey(query+APIs);
		 Entity entity = Entity.newBuilder(key)
		     .set("query",query)
		     .set("APIs", Arrays.toString(APIs))
		     .set("response", response)
		     .set("priority", 1)
		     .build();
		 datastore.put(entity);
		
	}
	
}
