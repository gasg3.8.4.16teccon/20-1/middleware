package lasalle.tcs.middleware;

import java.util.*;

public class API_Validator {
    
    List<String> listApis;

    API_Validator()
    {
        listApis = new ArrayList<String>(Arrays.asList("35.193.220.168/~lfloresr/APIproyecto/inglesapi.php?fuente=rapidApi&conjugacion=passive&palabra", "ahidalgogulasalle.appspot.com/translatR/API?parametros"));
    }

    public List<String> getApis()
    {    
        return listApis;
    }
}