package lasalle.tcs.middleware;

import java.io.IOException;
import java.io.InputStream;




import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.simple.JSONArray;

public class DS_Validator {

	
	public static InputStream getResource(String resource) throws Exception {
		   ClassLoader cl = Thread.currentThread().getContextClassLoader();
		   InputStream is = cl.getResourceAsStream(resource);
		   return is;
		}

	
	public static Boolean validateRequest(org.json.simple.JSONObject requestBODY) throws Exception
	{
		
		try (InputStream inputStream = getResource("/META-INF/request.json"))
		
		{
			  JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
			  Schema schema = SchemaLoader.load(rawSchema);
			  schema.validate(new JSONObject(requestBODY)); 
			  return true;// throws a ValidationException if this object is invalid
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				return false;
			}
		
		
	}



	@SuppressWarnings({ "unchecked", "null" })
	public static Object validateAPIResponses(JSONArray responses) throws Exception {
		// TODO Auto-generated method stub
		
		JSONArray returnResponses = null;
		for(int i = 0; i < responses.size(); i++)
		{
			if(validateResponse((org.json.simple.JSONObject) responses.get(i)))
			{
				returnResponses.add(responses.get(i));
			}
		}
		
		return returnResponses;
	}


	public static boolean validateResponse(org.json.simple.JSONObject jSONresponse) throws Exception {
try (InputStream inputStream = getResource("/META-INF/response.json"))
		
		{
			  JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
			  Schema schema = SchemaLoader.load(rawSchema);
			  schema.validate(new JSONObject(jSONresponse)); 
			  return true;// throws a ValidationException if this object is invalid
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				return false;
			}
		
	}
}

